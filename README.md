# r99: "99 Problems" in Rust

This repo contains Rust solutions to problems 1-10 of the
"99 Problems" suite, as adapted from the
[Haskell version](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems).
The code was primarily written by Redditor
[/u/PacoVelobs](http://reddit.com/u/PacoVelobs): I merely
"cleaned up" (may not be a cleanup) and adapted this code a
bit.

My changes are commented in the source; please see that for
details.

`/u/PacoVelobs` has graciously granted his work under the
"[WTFPL](https://en.wikipedia.org/wiki/WTFPL) version 2"
([Reddit comment](https://www.reddit.com/r/learnrust/comments/f1bvvo/code_review_10_questions/fh7551g)):
see the file `LICENSE` in this distribution for these
license terms. This work is made available under this
license also.
