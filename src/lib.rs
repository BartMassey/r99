//! Solutions to first 10 of "99 problems" of exercise.
//! Original solutions by Reddit /u/PacoVelobs.
//! Modified by Reddit /u/po8.

// po8: Comments on my changes are tagged in this style.

// po8: Renamed `vec` to `vals` throughout for clarity. Also
// made all the top-level comments doc comments.

/// Find the last element of a list.
pub fn my_last<T>(vals: &[T]) -> Option<&T> {
    // po8: Might as well just use the builtin.
    vals.last()
}

/// Find the last but one element of a list.
pub fn my_but_last<T>(vals: &[T]) -> Option<&T> {
    vals.split_last().and_then(|(_, x)| my_last(x))
}

/// Find the K'th element of a list. The first element in
/// the list is number 1.
pub fn element_at<T>(vals: &[T], index: usize) -> Option<&T> {
    // po8: Removed extra match arm and cleaned up a bit.
    match index {
        0 => None,
        i => vals.get(i - 1),
    }
}

/// Find the number of elements of a list.
pub fn my_length<T>(vals: &[T]) -> usize {
    // po8: Might as well just use the builtin.
    vals.len()
}

/// Reverse a list.
pub fn my_reverse<T: Clone>(vals: &[T]) -> Vec<T> {
    // po8: Could just use the builtin here, but let's just
    // clean up a bit using an iterator with `rev()`.
    let mut output = Vec::with_capacity(vals.len());
    for v in vals.iter().rev() {
        output.push(v.clone());
    }
    output
}

// po8: Renamed `is_palyndrome()` to `is_palindrome()`.
/// Find out whether a list is a palindrome. A palindrome
/// can be read forward or backward; e.g. (x a m a x).
pub fn is_palindrome<T: PartialEq>(vals: &[T]) -> bool {
    // po8: Converted to the functional style, which is
    // arguably less error-prone and might be faster.
    let nvals = vals.len();
    let half = nvals / 2;
    let left = vals[..half].iter();
    let right = vals[nvals - half..].iter().rev();
    left.eq(right)
}

/// Nested list structure.
pub enum NestedList<T> {
    /// Single element.
    Elem(T),
    /// Sublist.
    List(Vec<NestedList<T>>),
}

impl<T> NestedList<T> {
    // I've misplaced the username of the Redditor who
    // suggested this approach. Apologies.
    /// Get an iterator over the elements of a nested list
    /// in-order, "flattening" the list.
    pub fn flat<'a>(&'a self) -> Box<dyn Iterator<Item = &'a T> + 'a> {
        match self {
            NestedList::Elem(e) =>
                Box::new(std::iter::once(e)),
            NestedList::List(v) =>
                Box::new(v.iter().flat_map(NestedList::flat)),
        }
    }
}

impl<T: Clone> NestedList<T> {
    /// Return the elements of a nested list in-order,
    /// "flattening" it.
    pub fn flatten(&self) -> Vec<T> {
        self.flat().cloned().collect()
    }
}

// po8: This and subsequent should maybe return references
// instead of clones, in case one wants to use them on
// non-clone types. There's nothing about the datatypes that
// precludes this.
/// Eliminate consecutive duplicates of list elements.
///
/// Note: This may produce unexpected results when
/// `T` is not `Eq`.
///
/// ```
/// let nan = std::f32::NAN;
/// // `compress()` will produce a `Vec` containing
/// // two NaNs here, but there's no easy way to test
/// // for that since `nan != nan`.
/// assert_eq!(r99::compress(&[nan, nan]).len(), 2);
/// ```
pub fn compress<T: Clone + PartialEq>(vals: &[T]) -> Vec<T> {
    // po8: This would be cleaner and more efficient in
    // functional style, but the imperative might be easier
    // to understand.
    let mut result = Vec::new();
    let mut last = None;
    for v in vals {
        if Some(v) != last {
            result.push(v.clone());
            last = Some(v);
        }
    }
    result
}

/// Pack consecutive duplicates of list elements into
/// sublists. If a list contains repeated elements they
/// should be placed in separate sublists.
pub fn pack<T: Clone + PartialEq>(vals: &[T]) -> Vec<Vec<T>> {
    // po8: Restructured to use iterators instead of
    // indexing.
    let mut result = Vec::new();
    let mut vals = vals.iter().peekable();
    while let Some(v) = vals.next() {
        let mut cur = Vec::new();
        cur.push(v.clone());
        while let Some(&w) = vals.peek() {
            if w != v {
                break;
            }
            cur.push(w.clone());
            let _ = vals.next();
        }
        result.push(cur);
    }
    result
}

/// Run-length encoding of a list. Use the result of problem
/// P09 to implement the so-called run-length encoding data
/// compression method. Consecutive duplicates of elements
/// are encoded as lists (N E) where N is the number of
/// duplicates of the element E.
///
/// Note: The last element of each group will be returned.
pub fn encode<T: Clone + PartialEq>(vals: &[T]) -> Vec<(T, usize)> {
    // po8: Restructured a bit to avoid an extra `clone()`
    // per group.
    pack(&vals)
        .into_iter()
        .map(|mut v| {
            let n = v.len();
            let v = v.pop().unwrap();
            (v, n)
        })
        .collect()
}

#[cfg(test)]
mod test {
    // po8: Renamed all the tests to start with `test_`,
    // then imported `super` to avoid verbosity.
    use super::*;

    #[test]
    fn test_my_last() {
        let mut vec: Vec<i8> = Vec::new();
        assert_eq!(my_last(&vec), vec.last());

        vec.push(1);
        assert_eq!(my_last(&vec), vec.last());
    }

    #[test]
    fn test_my_but_last() {
        let mut vec: Vec<i8> = Vec::new();
        match vec.split_last() {
            None => assert_eq!(my_but_last(&vec), None),
            Some((_, xs)) => assert_eq!(my_but_last(&vec), xs.last()),
        }

        vec.push(0);
        match vec.split_last() {
            None => assert_eq!(my_but_last(&vec), None),
            Some((_, xs)) => assert_eq!(my_but_last(&vec), xs.last()),
        }

        vec.push(1);
        match vec.split_last() {
            None => assert_eq!(my_but_last(&vec), None),
            Some((_, xs)) => assert_eq!(my_but_last(&vec), xs.last()),
        }
    }

    #[test]
    fn test_element_at() {
        let mut vec: Vec<i8> = Vec::new();
        assert_eq!(element_at(&vec, 0), None);
        assert_eq!(element_at(&vec, 1), None);

        vec.push(0);
        assert_eq!(element_at(&vec, 0), None);
        assert_eq!(element_at(&vec, 1), vec.get(0));

        vec.push(1);
        assert_eq!(element_at(&vec, 0), None);
        assert_eq!(element_at(&vec, 1), vec.get(0));
    }

    #[test]
    fn test_my_length() {
        let mut vec: Vec<i8> = Vec::new();
        assert_eq!(my_length(&vec), vec.len());

        vec.push(0);
        assert_eq!(my_length(&vec), vec.len());

        vec.push(1);
        assert_eq!(my_length(&vec), vec.len());
    }

    #[test]
    fn test_my_reverse() {
        let vec = vec![1, 2, 3, 4, 5];
        let mut tmp = vec.clone();
        tmp.reverse();
        assert_eq!(my_reverse(&vec), tmp);
    }

    #[test]
    fn test_is_palindrome() {
        // po8: Added tests for short and odd/even lengths,
        // which are common bugs here.
        assert_eq!(is_palindrome(&"".as_bytes()), true);
        assert_eq!(is_palindrome(&"I".as_bytes()), true);
        assert_eq!(is_palindrome(&"kayak".as_bytes()), true);
        assert_eq!(is_palindrome(&"pullup".as_bytes()), true);
        assert_eq!(is_palindrome(&"foo".as_bytes()), false);
    }

    #[test]
    #[should_panic]
    // po8: Test fails since `nan != nan`.
    fn test_is_palindrome_partial() {
        let nan = std::f32::NAN;
        assert!(is_palindrome(&[nan, 0.0, nan]));
    }

    #[test]
    fn test_flatten() {
        use NestedList;

        // po8: Removed flat list tests due to changed
        // interface.
        let nested_list = NestedList::List(vec![
            NestedList::Elem(1),
            NestedList::Elem(2),
            NestedList::List(vec![
                NestedList::Elem(3),
                NestedList::List(vec![
                    NestedList::Elem(4),
                    NestedList::Elem(5),
                ]),
            ]),
        ]);
        assert_eq!(nested_list.flatten(), [1, 2, 3, 4, 5]);
    }

    #[test]
    fn test_compress() {
        let mut vec = vec![];
        assert_eq!(compress(&vec), []);

        vec.push(0);
        assert_eq!(compress(&vec), [0]);
        vec.push(0);
        assert_eq!(compress(&vec), [0]);
        vec.push(0);
        assert_eq!(compress(&vec), [0]);

        vec.push(1);
        assert_eq!(compress(&vec), [0, 1]);
        vec.push(1);
        assert_eq!(compress(&vec), [0, 1]);

        assert_eq!(
            compress(&"aaaabbbccccddeffffgg".as_bytes()),
            "abcdefg".as_bytes()
        );
    }

    #[test]
    fn test_pack() {
        let mut vec: Vec<u8> = vec![];
        let result: Vec<Vec<u8>> = vec![];

        assert_eq!(pack(&vec), result);

        vec.push(0);
        let result = vec![vec![0]];
        assert_eq!(pack(&vec), result);
        vec.push(0);
        let result = vec![vec![0, 0]];
        assert_eq!(pack(&vec), result);

        vec.push(1);
        let result = vec![vec![0, 0], vec![1]];
        assert_eq!(pack(&vec), result);
        vec.push(1);
        let result = vec![vec![0, 0], vec![1, 1]];
        assert_eq!(pack(&vec), result);
    }

    #[test]
    fn test_encode() {
        assert_eq!(
            encode(&vec![1, 1, 1, 2, 2, 2, 2, 3, 3, 4, 4, 4]),
            [(1, 3), (2, 4), (3, 2), (4, 3)]
        )
    }
}
